export default {
	state: {
		message: 'Vue component'
	},
	getters: {
		message (state) {
			return state.message;
		}
	},
	mutations: {},
	actions: {}
}
