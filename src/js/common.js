let maxProfit = function (prices) {
	return calculate(prices, 0);
};
console.log(maxProfit([71,11,51,31,61,41]));


function calculate(prices, index) {
	if (index >= prices.length) {
		return 0;
	}

	let maxProfit = 0;

	for (let start = index; start < prices.length; start++) {
		let localMaxProfit = 0;
		for (let i = start + 1; i < prices.length; i++) {
			if (prices[start] < prices[i]) {
				let profit = calculate(prices, i + 1) + prices[i] - prices[start];
				if (profit > localMaxProfit) {
					localMaxProfit = profit;
				}
			}
		}

		if (localMaxProfit > maxProfit)
			maxProfit = localMaxProfit;
	}
	return maxProfit;
}

module.exports = maxProfit;
