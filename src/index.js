// see more: https://github.com/vedees/webpack-template/blob/master/README.md#import-js-files

import './js/common.js'
import './assets/scss/main.scss'

import Vue from 'vue';
import store from './store';

Vue.component('example-component', require('./components/example.vue').default);

const app = new Vue({
	data: () => ({
		showComponent: false
	}),
	store,
	el: '#app'
});
